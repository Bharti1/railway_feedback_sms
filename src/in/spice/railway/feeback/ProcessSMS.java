package in.spice.railway.feeback;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import in.spice.railway.feedback.db.DatabaseManager;
import in.spice.railway.feedback.util.FetchNumbers;
import in.spice.railway.feedback.util.Log;
import in.spice.railway.feedback.util.SMS;

public class ProcessSMS implements Runnable {

	private String pnrNumber;
	private String seatNumber;
	private String trainNumber;
	private String coach;
	private String stationCode;
	private String msisdn;
	private String q1;
	private String q2;
	// private String q3;
	// private String q4;
	private String outdialId;
	private String obd;
	private String doj;
	private long transId;

	public ProcessSMS(String pnrNumber, String seatNumber, String trainNumber, String coach, String stationCode,
			String msisdn, String q1, String q2, String outdialId, String obd, String doj, long transId) {
		this.pnrNumber = pnrNumber;
		this.seatNumber = seatNumber;
		this.trainNumber = trainNumber;
		this.coach = coach;
		this.stationCode = stationCode;
		this.msisdn = msisdn;
		this.q1 = q1;
		this.q2 = q2;
		// this.q3 = q3;
		// this.q4 = q4;
		this.outdialId = outdialId;
		this.obd = obd;
		this.doj = doj;
		this.transId = transId;
		new Thread(this).start();
	}

	@Override
	public void run() {

		try {

			if (q1 != null && q1.equals("1") && obd.equals("0")) {
				String division = FetchNumbers.stationDivision.get(stationCode);
				SMS sms = new SMS(); // Unsatisfactory Station cleanliness

				try {
					List<String> recipients = FetchNumbers.stationCleanAndCateringNumbers.get(division);
					if (recipients != null && recipients.size() > 0) {
						sms.sendStn(FeedbackType.STNCLEANINESS, trainNumber, pnrNumber, msisdn, coach, seatNumber,
								recipients, stationCode, doj, transId);
						insertSmsLog(division, "Station Cleaniness", recipients);
					} else {
						Log.getNotExistLog("Division=" + division + ", in obd 0 and q1=0  recipient not exist");
					}
				} catch (IOException e) {
					Log.getErrorLog("in obd 0 catch q1=0 " + e.getMessage());
					e.printStackTrace();
				}
			}

			if (q2 != null && q2.equals("1") && obd.equals("2")) {
				String division = FetchNumbers.stationDivision.get(stationCode);
				SMS sms = new SMS(); // Unsatisfactory Station cleanliness

				try {
					List<String> recipients = FetchNumbers.trainPunctualityNumbers.get(division);
					if (recipients != null && recipients.size() > 0) {
						sms.send(FeedbackType.PUNCTUALITY, trainNumber, pnrNumber, msisdn, coach, seatNumber,
								recipients, doj, stationCode, transId);
						insertSmsLog(division, "Train Punctuality", recipients);
					} else {
						Log.getNotExistLog("Division=" + division + ", in obd 0 and q2=0 recipient not exist");
					}
				} catch (IOException e) {
					Log.getErrorLog("in obd 2 catch q2=0 " + e.getMessage());
					e.printStackTrace();
				}
			}

			if (q1 != null && q1.equals("1") && obd.equals("1")) {
				String division = FetchNumbers.trainDivision.get(trainNumber);
				SMS sms = new SMS();

				try {
					List<String> recipients = FetchNumbers.acCoolingNumbers.get(division);
					if (recipients != null && recipients.size() > 0) {
						sms.send(FeedbackType.ACCOOLING, trainNumber, pnrNumber, msisdn, coach, seatNumber, recipients,
								doj, stationCode, transId);
						insertSmsLog(division, "AC Cooling", recipients);
					} else {
						Log.getNotExistLog("Division=" + division + ", in obd 1 and q1=0 recipient not exist");
					}
				} catch (IOException e) {
					Log.getErrorLog("in obd 1 catch q1=0 " + e.getMessage());
					e.printStackTrace();
				}
			}

			if (q2 != null && q2.equals("1") && obd.equals("1")) {
				String division = FetchNumbers.trainDivision.get(trainNumber);
				SMS sms = new SMS();

				try {
					List<String> recipients = FetchNumbers.bedRollAndTrainCleanNumbers.get(division);
					if (recipients != null && recipients.size() > 0) {
						sms.send(FeedbackType.BEDROLL, trainNumber, pnrNumber, msisdn, coach, seatNumber, recipients,
								doj, stationCode, transId);
						insertSmsLog(division, "Bed Roll", recipients);
					} else {
						Log.getNotExistLog("Division=" + division + ", in obd 1 and q2=0 recipient not exist");
					}
				} catch (IOException e) {
					Log.getErrorLog("in obd 1 catch q2=0 " + e.getMessage());
					e.printStackTrace();
				}
			}
			if (q1 != null && q1.equals("1") && obd.equals("2")) {
				String division = null;

				List<String> divisionList= FetchNumbers.trainDivisionCateringList.get(trainNumber);

				if(divisionList!=null){

					for (String divisions : divisionList) {
						if(divisions.matches("IRCTC(.*)")){
							division=divisions;
						}
					}

					if(division==null){
						division=divisionList.get(0);
					}
				}

				SMS sms = new SMS();

				try {
					List<String> recipients = FetchNumbers.stationCleanAndCateringNumbers.get(division);
					if (recipients != null && recipients.size() > 0) {
						sms.send(FeedbackType.CATERING, trainNumber, pnrNumber, msisdn, coach, seatNumber, recipients,
								doj, stationCode, transId);
						insertSmsLog(division, "Catering", recipients);
					} else {
						Log.getNotExistLog("Division=" + division + ", in obd 2 and q1=0 recipient not exist");
					}
				} catch (IOException e) {
					Log.getErrorLog("in obd 2 catch q1=0 " + e.getMessage());
					e.printStackTrace();
				}
			}

			if (q2 != null && q2.equals("1") && obd.equals("0")) {
				String division = FetchNumbers.trainDivision.get(trainNumber);
				SMS sms = new SMS();

				try {
					List<String> recipients = FetchNumbers.bedRollAndTrainCleanNumbers.get(division);
					if (recipients != null && recipients.size() > 0) {
						sms.send(FeedbackType.CLEANLINESS, trainNumber, pnrNumber, msisdn, coach, seatNumber,
								recipients, doj, stationCode, transId);
						insertSmsLog(division, "Train Cleaniness", recipients);
					} else {
						Log.getNotExistLog("Division=" + division + ", in obd 2 and q2=0 recipient not exist");
					}
				} catch (IOException e) {
					Log.getErrorLog("in obd 2 catch q2=0 " + e.getMessage());
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			Log.getErrorLog("in ProcessSMS main catch block " + e.getMessage());
			e.printStackTrace();
		} finally {
			FeedbackSMS.intCounterThread--;
			if (FeedbackSMS.intCounterThread < 1) {
				Log.getAccessLog("in thread less than 1");
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					Log.getErrorLog("in catch block of ProcessSMS of thread counter");
					e.printStackTrace();
				}
			}
		}
	}

	public synchronized void insertSmsLog(String division, String keyword, List<String> mobileNumbers) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con.prepareStatement(
					"insert into feedback_sms_logs(train_number,pnr,msisdn,coach,seat_number,doj,station_code,division,date_time,keyword,pass_mobile) "
							+ "values(?,?,?,?,?,?,?,?,NOW(),?,?)");
			for (String mobileNumber : mobileNumbers) {
				stmt.setString(1, trainNumber);
				stmt.setString(2, pnrNumber);
				stmt.setString(3, mobileNumber);
				stmt.setString(4, coach);
				stmt.setString(5, seatNumber);
				stmt.setString(6, doj);
				stmt.setString(7, stationCode);
				stmt.setString(8, division);
				stmt.setString(9, keyword);
				stmt.setString(10, msisdn);
				stmt.executeUpdate();
				stmt.clearParameters();
			}
		} catch (Exception e) {
			Log.getErrorLog("in insertSmsLog catch block " + e.getMessage());
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}

	// public String getStringFromList(List<String> recipients) {
	// StringBuilder sb = new StringBuilder();
	// for (int i = 0; i < recipients.size(); i++) {
	// sb.append(recipients.get(i));
	//
	// if (i != recipients.size() - 1) {
	// sb.append(",");
	// }
	// }
	// return sb.toString();
	// }

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public long getTransId() {
		return transId;
	}

	public void setTransId(long transId) {
		this.transId = transId;
	}

}
