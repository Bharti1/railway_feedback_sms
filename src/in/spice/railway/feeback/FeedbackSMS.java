package in.spice.railway.feeback;

import in.spice.railway.feedback.db.DatabaseManager;
import in.spice.railway.feedback.util.FetchNumbers;
import in.spice.railway.feedback.util.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.dbutils.DbUtils;

public class FeedbackSMS {
	static int intCounterThread = 0;

	public static void main(String[] args) {
		FetchNumbers.stationDivision();
		FetchNumbers.trainDivision();
		FetchNumbers.acCoolingNumbers();
		FetchNumbers.bedRollAndTrainCleanNumbers();
		FetchNumbers.stationCleanAndCateringNumbers();
		FetchNumbers.trainPunctualityNumbers();
		FetchNumbers.trainDivisionCateringList();
		new FeedbackSMS().processFeedbackSMS();
	}

	// SELECT * FROM feedback_data "
	// + "WHERE (q1 = '3' OR q2 = '3' OR q3 = '3') AND sms_status = 'false' AND"
	// + " DATE_FORMAT(TIMESTAMP, '%Y-%m-%d')=?

	public void processFeedbackSMS() {
		Connection con = null;
		PreparedStatement stmt = null;
		PreparedStatement stmt2 = null;
		ResultSet rs = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String pnrNumber = "", seatNumber = "", trainNumber = "", coach = "", stationCode = "", msisdn = "", q1 = "",
				q2 = "", outdialId = "", obd = "", doj = "";

		try {
			con = DatabaseManager.getConnection();
			stmt = con.prepareStatement(
					"SELECT od.pnr_number,od.seat_number,od.train_no,od.coach,od.station_code,od.msisdn, "
							+ "fd.q1, fd.q2, fd.outdial_id, fd.obd, od.doj FROM "
							+ "feedback_data fd, tbl_outdial od WHERE (fd.q1 = '1' OR fd.q2 = '1') AND "
							+ "fd.sms_status = 'true' AND DATE_FORMAT(fd.TIMESTAMP, '%Y-%m-%d')=? "
							+ "AND od.id = fd.outdial_id limit 5");
			while (true) {
				stmt.setString(1, sdf.format(new Date()));
				rs = stmt.executeQuery();
				while (rs.next()) {
					pnrNumber = rs.getString("pnr_number");
					seatNumber = rs.getString("seat_number");
					trainNumber = rs.getString("train_no");
					coach = rs.getString("coach");
					stationCode = rs.getString("station_code");
					msisdn = rs.getString("msisdn");
					q1 = rs.getString("q1");
					q2 = rs.getString("q2");
					outdialId = rs.getString("outdial_id");
					obd = rs.getString("obd");
					doj = rs.getString("doj");
					long transId = System.currentTimeMillis();
					Log.getAccessLog("in while--- trans="+transId+"," + pnrNumber + "," + seatNumber + "," + trainNumber + "," + coach
							+ "," + stationCode + "," + msisdn + "," + q1 + "," + q2 + "," + outdialId + "," + obd + ","
							+ doj);
					if (checkThreadCounter()) {
						new ProcessSMS(pnrNumber, seatNumber, trainNumber, coach, stationCode, msisdn, q1, q2,
								outdialId, obd, doj,transId);
						stmt2 = con.prepareStatement("update feedback_data set sms_status = ? where outdial_id = ?");
						stmt2.setString(1, "false");
						stmt2.setString(2, outdialId);
						int status = stmt2.executeUpdate();
						Log.getDatabaseLog("updated status for transId="+transId+","+outdialId+", status="+status);
					}
				}
			}
		} catch (Exception e) {
			Log.getErrorLog("In FeedbackSMS catch block "+e.getMessage());
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(stmt2);
			DbUtils.closeQuietly(rs);
		}
	}

	public boolean checkThreadCounter() {
		try {
			while (true) {
				if (intCounterThread < 5) {
					intCounterThread++;
					System.out.println("----------------intCounterThread -----------" + intCounterThread);
					break;
				} else {
					Thread.sleep(100);
				}

			}
		} catch (Exception objException) {
			Log.getErrorLog("In FeedbackSMS catch block of counter "+objException.getMessage());
			objException.printStackTrace();
		}
		return true;
	}

}
