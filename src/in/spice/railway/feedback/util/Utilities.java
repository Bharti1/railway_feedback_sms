package in.spice.railway.feedback.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class Utilities {
	
	public static boolean isBlank(String s) {
		return (s == null || s.trim().length() == 0);
	}
	
	public static String fetchUrl(String location) throws IOException {
		String output = "";
		
		URL url = null;
		HttpURLConnection conn = null;
		InputStream is = null;
		
		try {
			url = new URL(location);
			conn = (HttpURLConnection) url.openConnection();
			
			is = conn.getInputStream();
			
			output = IOUtils.toString(is);
		}
		finally {
			IOUtils.closeQuietly(is);
			
			try {
				if (conn != null) {
					conn.disconnect();
				}
			}
			catch (Exception e) {
				// Ignore
			}
		}
		
		return output;
	}
	
	public static String implode(List<String> list, String separator) {
		if (list == null) {
			return null;
		}
		
		if (list.size() == 1) {
			return list.get(0);
		}
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			
			if (i != list.size() - 1) {
				sb.append(separator);
			}
		}
		
		return sb.toString();
	}
}
