/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.spice.railway.feedback.util;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 *
 * @author ch-e01124
 */
public class Log {

    private static synchronized void writeLog(String logString, String strSubDir) {
       // System.out.println("---------------in logging class JARRRR-------------");
        String strFileName = "";
        String strDateDir = "";
        try{
	            strDateDir = "/home/FeedbackLogs/";
        }catch(Exception e){
        	e.printStackTrace();
        }
    
        Calendar objCalendarRightNow = Calendar.getInstance();
        int intMonth = objCalendarRightNow.get(Calendar.MONTH) + 1;
        int intDate = objCalendarRightNow.get(Calendar.DATE);
        int intHour = objCalendarRightNow.get(Calendar.HOUR_OF_DAY);
        int intMinute = objCalendarRightNow.get(Calendar.MINUTE);
        int intSecond = objCalendarRightNow.get(Calendar.SECOND);
        int intMilliSecond = objCalendarRightNow.get(Calendar.MILLISECOND);
        String strYear = "" + objCalendarRightNow.get(Calendar.YEAR);
        strDateDir = strDateDir + "" + intDate + "-" + intMonth + "-" + strYear;
        createDateDir(strDateDir);
        strFileName = "" + strDateDir + "/" + strSubDir + ".log";
        try {
            FileWriter out = new FileWriter(strFileName, true);
            String strLogString = intHour + ":" + intMinute + ":" + intSecond + ":" + intMilliSecond + "," + logString + "\n";
            out.write(strLogString);
            out.close();
           // System.out.println("---------------creating dir-------------");

        } catch (Exception ex) {

            ex.printStackTrace();
           // System.out.println("---------------error in dir-------------");
            System.exit(0);
        }
    } 

    private static synchronized void createDateDir(String dateDir) {
        try {
            new File(dateDir).mkdirs();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static synchronized String getDate(long longVar) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");
        return formatter.format(longVar) + "";
    }

    public static synchronized void getAccessLog(String Trace) {
        writeLog(Trace, "AccessLogs");
    }
    
    public static synchronized void getNotExistLog(String Trace) {
        writeLog(Trace, "NotExistDivisionNumbersLogs");
    }

    public static synchronized void getDatabaseLog(String Database) {
        writeLog(Database, "Database");
    }

    public static synchronized void getErrorLog(String Error) {
        writeLog(Error, "Error");
    }
    public static synchronized void getCreditTransferRequest(String Error) {
        writeLog(Error, "CreditTransferRequest");
    }
    public static synchronized void getCreditTransferResult(String Error) {
        writeLog(Error, "CreditTransferResponse");
    }    
}
