package in.spice.railway.feedback.util;

import in.spice.railway.feeback.FeedbackType;
import in.spice.railway.feedback.config.ConfigurationManager;
import in.spice.railway.feedback.db.DatabaseManager;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbutils.DbUtils;

public class SMS {

	private final Logger logger = Logger.getLogger(this.getClass().getName());
	HashMap<String, List<String>> phoneNumbers = new HashMap<String, List<String>>();
	HashMap<FeedbackType, String> messageContent = new HashMap<FeedbackType, String>();
	// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
	// private static final String SQL_GET_STN_INFO =
	// "SELECT station_name,mobile_no FROM tbl_station_code_mobile_numbers WHERE
	// train_no=? AND station_code=?";
	private static final String SQL_INSERT_SMS_LOG = "INSERT INTO tbl_feedback_sms_logs (date_time,train_no,message_type,message_text,mobile,message_count) VALUES (NOW(),?,?,?,?,?)";

	public SMS() {
		init();
	}

	public void init() {

		messageContent = new HashMap<FeedbackType, String>();

		messageContent.put(FeedbackType.CATERING,
				"On %s In Train-%s Coach-%s Berth-%s & DOB-%s from %s Stn, Passenger has given Bad feedback for Catering Service. Pass Mobile#%s.");
		messageContent.put(FeedbackType.CLEANLINESS,
				"On %s In Train-%s Coach-%s Berth-%s & DOB-%s from %s Stn, Passenger has given Bad feedback for Train Cleanliness. Pass Mobile#%s");
		messageContent.put(FeedbackType.STNCLEANINESS,
				"On %s In Train-%s Coach-%s Berth-%s & DOB-%s from %s Stn, Passenger has given Bad feedback about cleanliness of %s Stn. Mobile#%s");

		messageContent.put(FeedbackType.ACCOOLING,
				"On %s In Train-%s Coach-%s Berth-%s & DOB-%s from %s Stn, Passenger has given Bad feedback for AC Cooling. Pass Mobile#%s");
		messageContent.put(FeedbackType.BEDROLL,
				"On %s In Train-%s Coach-%s Berth-%s & DOB-%s from %s Stn, Passenger has given Bad feedback for Bed Roll. Pass Mobile#%s");
		messageContent.put(FeedbackType.PUNCTUALITY,
				"On %s In Train-%s Coach-%s Berth-%s & DOB-%s from %s Stn, Passenger has given Abnormal delay Feedback for TR Punctuality. Pass Mobile#%s");

	}

	public void send(FeedbackType type, String trainNumber, String pnr, String userMsisdn, String coach, String berth,
			List<String> recipients, String doj, String station,long transId) throws UnsupportedEncodingException, IOException {
		ConfigurationManager cm = ConfigurationManager.getInstance();

		String dateStr = sdf.format(new Date());

		String messageFmt = messageContent.get(type);
		try {
			Date date1 = sdf1.parse(doj);

			doj = sdf.format(date1);
		} catch (Exception e) {
			Log.getErrorLog("in send catch block transId=" + transId + "," + e.getMessage());
			e.printStackTrace();
		}
		// String currentDate = sdf.format(new Date());
		// String messageText = String.format(messageFmt, trainNumber, dateStr,
		// pnr, userMsisdn, coach);
		String messageText = String.format(messageFmt, dateStr, trainNumber, coach, berth, doj, station, userMsisdn);

		String urlFmt = cm.getProperty("sms.api.url");
		System.out.println("messageText=== " + messageText);
		Log.getAccessLog("in send transId="+transId+", recepient size is="+recipients.size());
		if (recipients != null && recipients.size() > 0) {
			for (String recipient : recipients) {
				String url = String.format(urlFmt, recipient, URLEncoder.encode(messageText, "UTF-8"));
				Log.getAccessLog("in send transId=" + transId + "," + url);
				String urlResponse = Utilities.fetchUrl(url);
				Log.getAccessLog("in send urlReponse transId=" + transId + "," + urlResponse);
				SMSlogging(trainNumber, messageText, type, recipient);

				System.out.printf("URL Response: %s\n", urlResponse);
			}
		} else {
			Log.getAccessLog("in else of send transId="+transId+", No number found to send sms.");
			System.out.println("No number found to send sms");
		}
	}

	public void sendStn(FeedbackType type, String trainNumber, String pnr, String userMsisdn, String coach,
			String berth, List<String> recipients, String stationCode, String doj, long transId)
					throws UnsupportedEncodingException, IOException {
		ConfigurationManager cm = ConfigurationManager.getInstance();

		String dateStr = sdf.format(new Date());

		try {
			Date date1 = sdf1.parse(doj);

			doj = sdf.format(date1);
		} catch (Exception e) {
			Log.getErrorLog("in sendStn catch block transId=" + transId + "," + e.getMessage());
			e.printStackTrace();
		}

		String messageFmt = messageContent.get(type);
		// String messageText = String.format(messageFmt, trainNumber, dateStr,
		// pnr, userMsisdn, coach);
		String messageText = String.format(messageFmt, dateStr, trainNumber, coach, berth, doj, stationCode,
				stationCode, userMsisdn);

		String urlFmt = cm.getProperty("sms.api.url");
		System.out.println("messageText=== " + messageText);
		Log.getAccessLog("in sendStn transId="+transId+", recepient size is="+recipients.size());
		if (recipients != null && recipients.size() > 0) {
			for (String recipient : recipients) {
				String url = String.format(urlFmt, recipient, URLEncoder.encode(messageText, "UTF-8"));
				Log.getAccessLog("in sendStn transId=" + transId + "," + url);

				String urlResponse = Utilities.fetchUrl(url);
				Log.getAccessLog("in sendStn urlReponse transId=" + transId + "," + urlResponse);
				SMSlogging(trainNumber, messageText, type, recipient);

				System.out.printf("URL Response: %s\n", urlResponse);
			}
		} else {
			Log.getAccessLog("in else of sendStn transId="+transId+", No number found to send sms.");
			System.out.println("No number found to send sms");
		}
	}

	public void SMSlogging(String train_no, String message_text, FeedbackType message_type, String message_recipients) {
		int sms_parts = 0;
		int total_recipents = 0;
		int total_sms = 0;
		if (message_text.length() > 159) {
			sms_parts = 2;
		} else {
			sms_parts = 1;
		}
		total_recipents = message_recipients.length() - message_recipients.replace("$", "").length();
		total_recipents = total_recipents + 1;
		total_sms = total_recipents * sms_parts;
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DatabaseManager.getConnection();
			// SQL_INSERT_SMS_LOG = "INSERT INTO tbl_feedback_sms_logs
			// (date_time,train_no,message_type,message_text,mobile,message_count)
			// VALUES (NOW(),?,?,?,?,?)";

			st = conn.prepareStatement(SQL_INSERT_SMS_LOG);
			st.setString(1, train_no);
			st.setString(2, "" + message_type);
			st.setString(3, message_text);
			st.setString(4, message_recipients);
			st.setInt(5, total_sms);

			st.executeUpdate();
		} catch (SQLException e) {
			Log.getErrorLog("in SMSlogging catch block "+e.getMessage());
			e.printStackTrace();
			logger.log(Level.SEVERE, "DB error", e);
		} finally {
			DbUtils.closeQuietly(st);
			DbUtils.closeQuietly(conn);

		}

	}

}
