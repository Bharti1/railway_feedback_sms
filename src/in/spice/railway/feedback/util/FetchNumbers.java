package in.spice.railway.feedback.util;

import in.spice.railway.feedback.db.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

public class FetchNumbers {
	public static Map<String, String> stationDivision = new HashMap<String, String>();
	public static Map<String, String> trainDivision = new HashMap<String,String>();
	public static Map<String, List<String>> acCoolingNumbers = new HashMap<String, List<String>>();
	public static Map<String, List<String>> bedRollAndTrainCleanNumbers = new HashMap<String, List<String>>();
	public static Map<String, List<String>> stationCleanAndCateringNumbers = new HashMap<String, List<String>>();
	public static Map<String, List<String>> trainPunctualityNumbers = new HashMap<String, List<String>>();
	public static Map<String, List<String> >trainDivisionCateringList = new HashMap<String, List<String>>();

	public static void stationDivision() {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select station_code,division_name from division_stations");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String station = rs.getString(1);
				String division = rs.getString(2);
				if (!stationDivision.containsKey(station)) {
					stationDivision.put(station, division);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}
	
	public static void trainDivision() {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select train_number,division from division_train");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String train = rs.getString(1);
				String division = rs.getString(2);
				if (!trainDivision.containsKey(train)) {
					trainDivision.put(train, division);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}
	
	public static void acCoolingNumbers(){
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select division,mobile from tbl_ac_cooling_alert");
			rs = stmt.executeQuery();
			while(rs.next()){
				String division = rs.getString(1);
				String mobile = rs.getString(2);
				
				if(acCoolingNumbers.containsKey(division)){
					List<String> numbers = acCoolingNumbers.get(division);
					numbers.add(mobile);
				} else{
					List<String> numbers = new ArrayList<String>();
					numbers.add(mobile);
					acCoolingNumbers.put(division, numbers);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}  finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}
	
	public static void bedRollAndTrainCleanNumbers(){
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select division,mobile from tbl_bedroll_trainclean_alert");
			rs = stmt.executeQuery();
			while(rs.next()){
				String division = rs.getString(1);
				String mobile = rs.getString(2);
				
				if(bedRollAndTrainCleanNumbers.containsKey(division)){
					List<String> numbers = bedRollAndTrainCleanNumbers.get(division);
					numbers.add(mobile);
				} else{
					List<String> numbers = new ArrayList<String>();
					numbers.add(mobile);
					bedRollAndTrainCleanNumbers.put(division, numbers);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}  finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}
	
	public static void stationCleanAndCateringNumbers(){
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select division,mobile from tbl_stnclean_catering_alert");
			rs = stmt.executeQuery();
			while(rs.next()){
				String division = rs.getString(1);
				String mobile = rs.getString(2);
				
				if(stationCleanAndCateringNumbers.containsKey(division)){
					List<String> numbers = stationCleanAndCateringNumbers.get(division);
					numbers.add(mobile);
				} else{
					List<String> numbers = new ArrayList<String>();
					numbers.add(mobile);
					stationCleanAndCateringNumbers.put(division, numbers);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}  finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}
	
	public static void trainPunctualityNumbers(){
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select division,mobile from tbl_train_punctuality_alert");
			rs = stmt.executeQuery();
			while(rs.next()){
				String division = rs.getString(1);
				String mobile = rs.getString(2);
				
				if(trainPunctualityNumbers.containsKey(division)){
					List<String> numbers = trainPunctualityNumbers.get(division);
					numbers.add(mobile);
				} else{
					List<String> numbers = new ArrayList<String>();
					numbers.add(mobile);
					trainPunctualityNumbers.put(division, numbers);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}  finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}
	
	public static void trainDivisionCateringList() {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = DatabaseManager.getConnection();
			stmt = con
					.prepareStatement("select train_number,division from division_train");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String train = rs.getString(1);
				String division = rs.getString(2);


				if(trainDivisionCateringList.containsKey(train)){
					List<String> divisions = trainDivisionCateringList.get(train);
					divisions.add(division);
				} else{
					List<String> divisions = new ArrayList<String>();
					divisions.add(division);
					trainDivisionCateringList.put(train, divisions);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(con);
		}
	}

}
